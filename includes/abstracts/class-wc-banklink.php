<?php
abstract class WC_Banklink extends WC_Payment_Gateway {
	/**
	 * WC_Banklink
	 */
	function __construct() {
		// Get icon and set notification URL.
		$this->icon       = $this->get_option( 'logo', plugins_url( 'assets/img/' . $this->id . '.png', WC_CABALLERO_GATEWAYS_MAIN_FILE ) );
		$this->has_fields = false;
		$this->notify_url = WC()->api_request_url( get_class( $this ) );

		// Get the settings.
		$this->title       = $this->get_option( 'title' );
		$this->enabled     = $this->get_option( 'enabled' );
		$this->description = $this->get_option( 'description' );

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();

		// Payment listener/API hook.
		add_action( 'woocommerce_api_' . strtolower( esc_attr( get_class( $this ) ) ), array( $this, 'check_bank_response' ) );

		// Actions.
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_receipt_' . $this->id, array( $this, 'receipt_page' ) );
		add_action( 'woocommerce_' . $this->id . '_check_response', array( $this, 'validate_bank_response' ) );
	}

	function bcmod( $x, $y ) {
		// how many numbers to take at once? carefull not to exceed (int)
		$take = 5;
		$mod  = '';

		do {
			$a   = (int) $mod . substr( $x, 0, $take );
			$x   = substr( $x, $take );
			$mod = $a % $y;
		} while ( strlen( $x ) );

		return (int) $mod;
	}

	/**
	 * Set settings fields
	 *
	 * @return void
	 */
	function init_form_fields() {
		// Set fields.
		$this->form_fields = array(
			'enabled'     => array(
				'title'   => __( 'Enable banklink', 'wc-gateway-caballero-banklink' ),
				'type'    => 'checkbox',
				'default' => 'no',
				'label'   => __( 'Enable this payment gateway', 'wc-gateway-caballero-banklink' ),
			),
			'title'       => array(
				'title'       => __( 'Title', 'wc-gateway-caballero-banklink' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which user sees during checkout.', 'wc-gateway-caballero-banklink' ),
				'default'     => $this->get_title(),
				'desc_tip'    => true,
			),
			'description' => array(
				'title'       => __( 'Customer message', 'wc-gateway-caballero-banklink' ),
				'type'        => 'textarea',
				'default'     => '',
				'description' => __( 'This will be visible when user selects this payment gateway during checkout.', 'wc-gateway-caballero-banklink' ),
				'desc_tip'    => true,
			),
			'logo'        => array(
				'title'       => __( 'Logo', 'wc-gateway-caballero-banklink' ),
				'type'        => 'text',
				'default'     => $this->icon,
				'description' => __( 'Enter full URL to set a custom logo. You could upload the image to your media library first.', 'wc-gateway-caballero-banklink' ),
				'desc_tip'    => true,
			),
			'countries'   => array(
				'title'       => __( 'Country availability', 'wc-gateway-caballero-banklink' ),
				'type'        => 'multiselect',
				'class'       => 'wc-enhanced-select',
				'options'     => array_merge(
					array( 'all' => __( 'All countries', 'wc-gateway-caballero-banklink' ) ),
					WC()->countries->get_countries()
				),
				'default'     => array( 'all' ),
				'description' => __( 'Specify countries where this method should be available. Select only "all countries" to sell everywhere.', 'wc-gateway-caballero-banklink' ),
				'desc_tip'    => true,
			),
		);
	}

	/**
	 * Generate some kind of reference number
	 *
	 * @param  string $stamp Purchase ID.
	 * @return string        Reference number
	 */
	function generate_ref_num( $stamp ) {
		// Replace chars with nums.
		$char_table = array(
			'A' => 10,
			'B' => 11,
			'C' => 12,
			'D' => 13,
			'E' => 14,
			'F' => 15,
			'G' => 16,
			'H' => 17,
			'I' => 18,
			'J' => 19,
			'K' => 20,
			'L' => 21,
			'M' => 22,
			'N' => 23,
			'O' => 24,
			'P' => 25,
			'Q' => 26,
			'R' => 27,
			'S' => 28,
			'T' => 29,
			'U' => 30,
			'V' => 31,
			'W' => 32,
			'X' => 33,
			'Y' => 34,
			'Z' => 35,
		);
		// Normalize.
		$normalized_ref = strtoupper( preg_replace( '/\s+/', '', $stamp ) );
		// Calc.
		// add 'RF00' to the end of ref.
		$pre_result = $normalized_ref . 'RF00';
		// Replace to numeric.
		$pre_result = str_replace( array_keys( $char_table ), array_values( $char_table ), strtoupper( $pre_result ) );
		// Calculate checksum.
		$checksum = ( 98 - $this->bcmod( $pre_result, '97' ) );
		// pad to 2 digits if under 10.
		$checksum = str_pad( $checksum, 2, '0', STR_PAD_LEFT );

		$rf_ref = 'RF' . $checksum . $normalized_ref;
		return trim( $rf_ref );
	}

	/**
	 * Payment processing
	 *
	 * @param  integer $order_id Order ID
	 * @return array             Redirect URL and result (success)
	 */
	function process_payment( $order_id ) {
		// Get the order.
		$order = wc_get_order( $order_id );

		// Redirect.
		return array(
			'result'   => 'success',
			'redirect' => $order->get_checkout_payment_url( true ),
		);
	}

	/**
	 * Adds form to the receipt page
	 *
	 * @param  integer $order_id Order ID
	 * @return void
	 */
	function receipt_page( $order_id ) {
		// Say thank you :).
		echo apply_filters( 'the_content', sprintf( __( 'Thank you for your order, please click the button below to pay with %s in case automatic redirection does not work.', 'wc-gateway-caballero-banklink' ), $this->get_title() ) );

		// Generate the form.
		echo $this->output_gateway_redirection_form( $order_id );
	}

	/**
	 * Is this gateway available?
	 *
	 * @return boolean
	 */
	function is_available() {
		if ( WC()->customer == null ) {
			return false;
		}

		return $this->get_option( 'enabled', 'no' ) != 'no' && array_intersect( array( 'all', wc_caballero_gateways_get_customer_billing_country() ), $this->get_option( 'countries' ) );
	}

	/**
	 * Get default language code
	 *
	 * @return string Language code
	 */
	function get_default_language() {
		$locale = get_locale();

		if ( strlen( $locale ) > 2 ) {
			$locale = substr( $locale, 0, 2 );
		}

		return $locale;
	}

	/**
	 * Easier debugging
	 *
	 * @param  mixed $data Data to be saved
	 * @return void
	 */
	function debug( $data, $level = 'debug' ) {
		if ( defined( 'WP_DEBUG_LOG' ) && WP_DEBUG_LOG === true ) {
			$log_data = is_array( $data ) || is_object( $data ) ? print_r( $data, true ) : var_export( $data, true );

			if ( function_exists( 'wc_get_logger' ) ) {
				$logger = wc_get_logger();
				$logger->log( $level, $log_data, array( 'source' => $this->id ) );
			} else {
				$logger = new WC_Logger();
				$logger->add( $this->id, $log_data );
			}
		}
	}

	/**
	 * Easier logging
	 *
	 * @param  mixed $data Data to be saved
	 * @return void
	 */
	function log( $data, $level = 'info' ) {
		$log_data = is_array( $data ) || is_object( $data ) ? print_r( $data, true ) : var_export( $data, true );

		if ( function_exists( 'wc_get_logger' ) ) {
			$logger = wc_get_logger();
			$logger->log( $level, $log_data, array( 'source' => $this->id ) );
		} else {
			$logger = new WC_Logger();
			$logger->add( $this->id, $log_data );
		}
	}

	/**
	 * Creates a filter for altering transaction data
	 *
	 * @param  array    $data  Transaction data.
	 * @param  WC_Order $order Order.
	 * @return array           Modified transaction data
	 */
	function hookable_transaction_data( $data, $order ) {
		return apply_filters( 'woocommerce_' . $this->id . '_gateway_transaction_fields', $data, $order );
	}

	/**
	 * Generate form that redirects users to bank
	 *
	 * @param  string $url    URL to redirect.
	 * @param  array  $fields Fields to include as hidden inputs.
	 * @return string         HTML for the form
	 */
	function get_redirect_form( $url, $fields ) {
		// Start form.
		$form = sprintf( '<form action="%s" method="post" id="banklink_%s_submit_form">', esc_attr( $url ), $this->id );

		// Add fields to form inputs.
		foreach ( $fields as $name => $value ) {
			$form .= sprintf( '<input type="hidden" name="%s" value="%s" />', esc_attr( $name ), htmlspecialchars( $value ) );
		}

		// Show "Pay" button and end the form.
		$form .= sprintf( '<input type="submit" name="send_banklink" class="button" value="%s">', __( 'Pay', 'wc-gateway-caballero-banklink' ) );
		$form .= '</form>';

		// Debug output.
		$this->debug( $fields );

		// Add inline JS.
		//wc_enqueue_js( sprintf( 'jQuery( "#banklink_%s_submit_form" ).submit();', $this->id ) );
		//Try to avoid double submit if browser back button is used
		wc_enqueue_js( sprintf( 'window.setTimeout(function(){jQuery( "#banklink_%s_submit_form" ).submit();}, 3000);', $this->id ) );

		return apply_filters( sprintf( 'woocommerce_%s_gateway_redirect_form_html', $this->id ), $form, $fields );
	}
}
