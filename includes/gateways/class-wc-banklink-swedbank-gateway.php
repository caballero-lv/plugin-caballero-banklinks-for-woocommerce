<?php
/**
 * Swedbank gateway
 *
 * @package BankLink
 */

?>
<?php
/**
 * Swedbank gateway class
 */
class WC_Banklink_Swedbank_Gateway extends WC_Banklink_Ipizza {
	/**
	 * WC_Banklink_Swedbank_Gateway
	 */
	function __construct() {
		$this->id           = 'swedbank';
		$this->method_title = __( 'Swedbank', 'wc-gateway-caballero-banklink' );

		parent::__construct();
	}
}
