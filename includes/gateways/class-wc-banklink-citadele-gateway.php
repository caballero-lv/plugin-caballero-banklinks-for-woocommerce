<?php
/**
 * Citadele gateway
 *
 * @package BankLink
 */

?>
<?php
/**
 * Citadele gateway class
 */
class WC_Banklink_Citadele_Gateway extends WC_Banklink_Ipizza {
	/**
	 * WC_Banklink_Citadele_Gateway
	 */
	function __construct() {
		$this->id           = 'citadele';
		$this->method_title = __( 'Citadele', 'wc-gateway-caballero-banklink' );

		parent::__construct();
	}
}
