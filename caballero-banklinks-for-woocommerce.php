<?php
/**
 * Plugin Name: Caballero Banklinks for WooCommerce
 * Description: Extends WooCommerce with most commonly used banklinks.
 * Author:      Caballero
 * Author URI:  https://www.caballero.lv
 * Version:     1.1.8
 */

?>
<?php
// Security check.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Main file constant
 */
define( 'WC_CABALLERO_GATEWAYS_MAIN_FILE', __FILE__ );

/**
 * Includes folder path
 */
define( 'WC_CABALLERO_GATEWAYS_INCLUDES_PATH', plugin_dir_path( WC_CABALLERO_GATEWAYS_MAIN_FILE ) . 'includes' );

/**
 * @class    Caballero_Gateways_For_WooCommerce
 * @category Plugin
 * @package  Caballero_Gateways_For_WooCommerce
 */
class Caballero_Gateways_For_WooCommerce {
	/**
	 * Instance
	 *
	 * @var null
	 */
	private static $instance = null;

	/**
	 * Class constructor
	 */
	function __construct() {
		add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );

		// Allow WC template file search in this plugin
		add_filter( 'woocommerce_locate_template', array( $this, 'locate_template' ), 20, 3 );
		add_filter( 'woocommerce_locate_core_template', array( $this, 'locate_template' ), 20, 3 );
	}

	/**
	 * Initialize plugin
	 *
	 * @return void
	 */
	public function plugins_loaded() {
		// Check if payment gateways are available.
		if ( $this->is_payment_gateway_class_available() ) {
			add_filter( 'woocommerce_payment_gateways', array( $this, 'register_gateways' ) );

			// Load functionality, translations.
			$this->includes();
			$this->load_translations();
		}
	}

	/**
	 * Enqueue scripts
	 *
	 * @return void
	 */
	public function wp_enqueue_scripts() {

	}

	/**
	 * Require functionality
	 *
	 * @return void
	 */
	public function includes() {
		// Compatibility helpers.
		require_once WC_CABALLERO_GATEWAYS_INCLUDES_PATH . '/compatibility-helpers.php';

		// Abstract classes.
		require_once WC_CABALLERO_GATEWAYS_INCLUDES_PATH . '/abstracts/class-wc-banklink.php';
		require_once WC_CABALLERO_GATEWAYS_INCLUDES_PATH . '/abstracts/class-wc-banklink-ipizza.php';

		// IPizza.
		require_once WC_CABALLERO_GATEWAYS_INCLUDES_PATH . '/gateways/class-wc-banklink-danske-gateway.php';
		require_once WC_CABALLERO_GATEWAYS_INCLUDES_PATH . '/gateways/class-wc-banklink-seb-gateway.php';
		require_once WC_CABALLERO_GATEWAYS_INCLUDES_PATH . '/gateways/class-wc-banklink-swedbank-gateway.php';
		require_once WC_CABALLERO_GATEWAYS_INCLUDES_PATH . '/gateways/class-wc-banklink-citadele-gateway.php';
		require_once WC_CABALLERO_GATEWAYS_INCLUDES_PATH . '/gateways/class-wc-banklink-nordea-ipizza-gateway.php';
	}

	/**
	 * Check if WooCommerce WC_Payment_Gateway class exists
	 *
	 * @return boolean True if it does
	 */
	function is_payment_gateway_class_available() {
		return class_exists( 'WC_Payment_Gateway' );
	}

	/**
	 * Load translations
	 *
	 * Allows overriding the offical translation by placing
	 * the translation files in wp-content/languages/caballero-banklinks-for-woocommerce
	 *
	 * @return void
	 */
	function load_translations() {
		$domain = 'wc-gateway-caballero-banklink';
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, WP_LANG_DIR . '/caballero-banklinks-for-woocommerce/' . $domain . '-' . $locale . '.mo' );
		load_plugin_textdomain( $domain, false, dirname( plugin_basename( WC_CABALLERO_GATEWAYS_MAIN_FILE ) ) . '/languages/' );
	}

	/**
	 * Register gateways
	 *
	 * @param  array $gateways Gateways
	 * @return array           Gateways
	 */
	function register_gateways( $gateways ) {
		$gateways[] = 'WC_Banklink_Danske_Gateway';
		$gateways[] = 'WC_Banklink_Swedbank_Gateway';
		$gateways[] = 'WC_Banklink_SEB_Gateway';
		$gateways[] = 'WC_Banklink_Citadele_Gateway';
		$gateways[] = 'WC_Banklink_Nordea_Ipizza_Gateway';

		return $gateways;
	}


	/**
	 * Fetch instance of this plugin
	 *
	 * @return Caballero_Gateways_For_WooCommerce
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Locates the WooCommerce template files from this plugin directory
	 *
	 * @param  string $template      Already found template
	 * @param  string $template_name Searchable template name
	 * @param  string $template_path Template path
	 * @return string                Search result for the template
	 */
	function locate_template( $template, $template_name, $template_path ) {
		// Tmp holder
		$_template = $template;

		if ( ! $template_path ) {
			$template_path = WC_TEMPLATE_PATH;
		}

		// Set our base path.
		$plugin_path = plugin_dir_path( WC_CABALLERO_GATEWAYS_MAIN_FILE ) . '/woocommerce/';

		// Look within passed path within the theme - this is priority.
		$template = locate_template(
			array(
				trailingslashit( $template_path ) . $template_name,
				$template_name,
			)
		);

		// Get the template from this plugin, if it exists.
		if ( ! $template && file_exists( $plugin_path . $template_name ) ) {
			$template = $plugin_path . $template_name;
		}

		// Use default template.
		if ( ! $template ) {
			$template = $_template;
		}

		// Return what we found.
		return $template;
	}
}


/**
 * Returns the main instance of Caballero_Gateways_For_WooCommerce to prevent the need to use globals.
 *
 * @return Caballero_Gateways_For_WooCommerce
 */
function WC_Caballero_Gateways() {
	return Caballero_Gateways_For_WooCommerce::instance();
}

// Global for backwards compatibility.
$GLOBALS['wc_caballero_gateways'] = WC_Caballero_Gateways();
